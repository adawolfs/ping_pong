var express = require('express');
var middlewares = require('./middlewares/admin')
var jade = require('jade')
var http = require('http')
var io = require('socket.io')
function moveBars(socket){
		console.log('io')
		socket.on('marginPlayer1', function(margin){
			console.log('PLAYER1')
			io.emit('marginPlayer1', margin);
		});
		socket.on('marginPlayer2', function(margin){
			console.log('PLAYER2')
			io.emit('marginPlayer2', margin);
		});
	}
var ExpressServer = function(config){
	config = config || {};
	this.expressServer = express();
	http = http.Server(this.expressServer);
	io = io(http)
	for(var middleware in middlewares){
		this.expressServer.use(middlewares[middleware])
	}
	this.expressServer.engine('html',jade.renderFile)
	this.expressServer.set('views', __dirname + '/website/views');
	this.expressServer.set('view engine', 'html');
	var player = '';
	var otherPlayer = '';
	var n = 0;
	this.expressServer.get('/',function(req,res){
		console.log('New Player');
		if(player =='Player1'){
			player = 'Player2';
			otherPlayer = 'Player1';
		} else {
			player = 'Player1'
			otherPlayer = 'Player2'
		}
		res.render('index.ejs',{player:player,otherPlayer:otherPlayer});
	})
	io.on('connection', function(socket){
		moveBars(socket)
	});
	this.startServer = function(){
		http.listen(config.port, function(){
			console.log('Server Started at port: ' + config.port)
		});
	}
}

module.exports = ExpressServer;